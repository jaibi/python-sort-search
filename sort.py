###################################################
### My implementation of a merge sort algorithm ###
###################################################
def MergeSort (arr, cb = lambda a,b: a > b) :

	# On éclate les valeurs en listes uniques de valeurs
	arr = [[i] for i in arr]

	# Tant que la liste ne contient pas un élément (le dernier, trié)
	while (len(arr) > 1):

		# print (arr)
		temp_arr = []
		# for subitem1, subitem2 in zip(arr[::2], arr[1::2]):
		# Pour chaque paire de sous-éléments du tableau
		for i in range(0, len(arr), 2):
			# Si i est à la fin du tableau et donc qu'il n'est pas possible de faire une paire
			if (i == len(arr) -1):
				# On ajoute cette paire à la liste pour la prochaine itération
				temp_arr.append(arr[-1])
				# Et on enchaîne
				break

			subitem1 = arr[i]
			subitem2 = arr[i + 1]

			temp_item = []
			index1 = 0
			index2 = 0

			# Tant que la taille du tableau de valeur est inférieure à la somme des éléments des deux sous-éléments
			while (len(temp_item) < (len(subitem1) + len(subitem2))):

				# Si item1 > item 2
				if (cb(subitem1[index1], subitem2[index2])):
					# Insertion de item 2
					temp_item.append(subitem2[index2])
					# Incrément index 2
					index2 += 1

					# Si item 2 était le dernier élément
					if (index2 > len(subitem2) - 1):
						# J'insère tous les éléments restants de subitem 1
						temp_item.extend(subitem1[index1:])

				# Sinon, on fait la même chose dans l'autre sens
				else:
					# Insertion de item 1
					temp_item.append(subitem1[index1])
					#Incrément index1
					index1 += 1

					# Si item 1 était le dernier élément
					if (index1 > len(subitem1) - 1):
						# J'insère tous les éléments restants de subitem 2
						temp_item.extend(subitem2[index2:])
				# print ("\t", temp_item)

			# On ajoute le merge des deux paires dans le tableau d'éléments
			temp_arr.append(temp_item)

		# Si le dernier sous-élément traité n'est pas le dernier de la liste (quand on utilise zip() pour faire les paires )
		# if (subitem2 != arr[-1]):
			# Alors on l'ajoute à la liste pour qu'il soit traité avec les autres au prochain tour
			# temp_arr.append(arr[-1])

		# print ("\n")
		arr = temp_arr

	return arr[0]




#################################
### BubbleSort implementation ###
#################################
def BubbleSort (arr, cb = lambda a,b: a > b) :
	for i in range(len(arr)):
		for j in range(i+1,len(arr)):
			if(cb(arr[i], arr[j])):
				temp=arr[i]
				arr[i]=arr[j]
				arr[j]=temp
	return arr



####################################
### SelectionSort implementation ###
####################################

def SelectionSort (arr, cb = lambda a,b: a > b) :
	for i in range(len(arr)):
		min = i
		for j in range(i+1, len(arr)):
			if cb(arr[min], arr[j]):
				min = j

		arr[i], arr[min] = arr[min], arr[i]
	return arr



################################
### QuickSort implementation ###
################################

def QuickSort(arr = [], cb = lambda a,b: a > b, start = 0, end = None):
	if (end == None):
		end = len(arr) - 1

	if start >= end:
		return arr

	(arr, p) = partition(arr, start, end, cb)
	arr = QuickSort(arr, cb, start, p-1)
	return QuickSort(arr, cb, p+1, end)


def partition(arr, start, end, cb):
	pivot = arr[start]
	low = start + 1
	high = end

	while True:
		while low <= high and cb(arr[high], pivot):
			high = high - 1

		while low <= high and not cb(arr[low], pivot):
			low = low + 1

		if low <= high:
			arr[low], arr[high] = arr[high], arr[low]
		else:
			break

	arr[start], arr[high] = arr[high], arr[start]

	return (arr,high)




##############################
### TimSort implementation ###
##############################

minrun = 32
def InsSort(arr,start,end, cb):
	for i in range(start+1,end+1):
		elem = arr[i]
		j = i-1
		while j>=start and not cb(elem, arr[j]):
			arr[j+1] = arr[j]
			j -= 1
		arr[j+1] = elem
	return arr

def merge(arr,start,mid,end):
	if mid==end:
		return arr
	first = arr[start:mid+1]
	last = arr[mid+1:end+1]
	len1 = mid-start+1
	len2 = end-mid
	ind1 = 0
	ind2 = 0
	ind	 = start

	while ind1<len1 and ind2<len2:
		if first[ind1]<last[ind2]:
			arr[ind] = first[ind1]
			ind1 += 1
		else:
			arr[ind] = last[ind2]
			ind2 += 1
		ind += 1

	while ind1<len1:
		arr[ind] = first[ind1]
		ind1 += 1
		ind += 1

	while ind2<len2:
		arr[ind] = last[ind2]
		ind2 += 1
		ind += 1

	return arr

def TimSort(arr, cb = lambda a,b: a > b):
	n = len(arr)

	for start in range(0,n,minrun):
		end = min(start+minrun-1,n-1)
		arr = InsSort(arr,start,end, cb)

	curr_size = minrun
	while curr_size<n:
		for start in range(0,n,curr_size*2):
			mid = min(n-1,start+curr_size-1)
			end = min(n-1,mid+curr_size)
			arr = merge(arr,start,mid,end)
		curr_size *= 2
	return arr

