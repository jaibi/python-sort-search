import time
import compare

def BinarySearch (needle, haystack, start, end, cb = lambda a,b : a > b):

	middle = start + ((end - start) // 2)
	if (end >= start):
		if (needle == haystack[middle]):
			# print ('trouvé')
			return (middle, True)
		elif (cb(needle, haystack[middle])) :
			# print ('right')
			return BinarySearch (needle, haystack, middle + 1, end, cb)
		else:
			# print ('left')
			return BinarySearch (needle, haystack, start, middle - 1, cb)
	else:
		return (middle, False)

def WordsInListSearch (sortedwords, sortedterms, cb = lambda a,b: a > b) :
	minlen = len(min(sortedterms))
	matches = []
	start = 0
	current = ''
	# Pour chaque mot de l'entrée utilisateur triée
	for word in sortedwords:
		# Si le mot est différent du précédent, et d'une longueur supérieure ou égale à la longueur minimum des mots de la liste de termes
		if (current != word and len(word) >= minlen):
			# On récupère la position où la recherche s'est arrêtée, qu'elle soit un succès ou non
			# Pour que la recherche reprenne à partir de cette position dans la liste de termes
			(start, found) = BinarySearch(word, sortedterms, start, len(sortedterms) -1, cb)
			if (found):
				# Si le mot est trouvé, on l'ajoute à la liste des résultats
				matches.append(word)
		# Sauvegarde du mot en cours, pour la prochaine itération
		current = word
	return matches

def FindBestPath (graph, start, end):

	bestCost = 10000
	bestPath = None
	if (start in graph and end in graph):

		currentPath = ''
		currentCost = 0
		# Afin de visualiser l'algorithme parcourir le graph
		paths = []
		allPaths = []

		#Initialisation du début de la recherche, en indiquant le premier noeud à partir duquel faire la recherche de chemin
		toVisit = [(start, start, 0)]
		# Comptabilisation du nombre de visites de noeuds
		visits = 0
		while (len(toVisit)):
			nextVisit = []
			for (node, path, cost) in toVisit:
				if (node in graph):
					for edge, edgeCost in graph[node]:
						if (edge not in path):
							currentCost = cost + edgeCost
							currentPath = path + edge

							if (currentCost > bestCost):
								continue

							if (edge == end):
								if (currentCost < bestCost):
									bestCost = currentCost
									bestPath = currentPath

								paths.append({'from':currentPath, 'cost':currentCost})

							allPaths.append({'from':currentPath, 'cost':currentCost})

							nextVisit.append((edge, currentPath, currentCost))
							visits += 1

			toVisit = nextVisit

		for path in paths:
			print (path)

		print ('')

		for path in allPaths:
			print (path)

		print ('Visites : ', visits)

	else:
		print('node', start, 'introuvable')

	return (bestPath, bestCost)
