import sys
import os
import random
import time

import sort

import compare
from search import WordsInListSearch, FindBestPath
from terms import TermsToBlock

clear_cmd = 'cls' if os.name == 'nt' else 'clear'

def execute (funcName, values, extract=10):
	compare.calls = 0
	start = time.time()
	sorted = getattr(sort, funcName)(values[::], compare.call)
	print("{} : {:0.4f} secondes, {} comparaisons".format(funcName, time.time() - start, compare.calls))
	print("Extrait : ", sorted[:extract], '\n')
	return sorted

while (True):
	os.system(clear_cmd)
	choice = input("Voulez-vous faire :\n\t[1] un trie\n\t[2] une recherche de texte\n\t[3] Recherche Graph\n\t[q] terminer le programme \n")
	os.system(clear_cmd)
	if (choice == "1"):
		choice = input("Voulez-vous trier \n\t[1] des nombres\n\t[2] une liste de mots\n")
		os.system(clear_cmd)
		if (choice == "1"):
			size = int(input("Taille de la liste de nombres à générer : "))
			start = time.time()
			values = random.sample(range(-size, size), size)
			os.system(clear_cmd)

		else:
			terms = TermsToBlock()
			factor = input(f"La liste fait {len(terms)} mots de long. Par combien voulez-vous la multiplier ? (par défaut : 1) ")
			factor = int(factor) if factor and int(factor) > 1 else 1
			start = time.time()
			values = random.sample(terms*factor, len(terms)*factor)

		print("Génération de la liste en {:0.4f} secondes - Nombre d'items dans la liste : {}".format(time.time() - start, len(values)))
		print("Extrait : ", values[:10], '\n')


		choice = input("Trie\n\t[1] Lents + Rapides\n\t[2] Rapides\n")
		if (choice == "1"):
			execute('BubbleSort', values)
			execute('SelectionSort', values)

		quicksorted = execute('QuickSort', values)
		timsorted = execute('TimSort', values)
		mergesorted = execute('MergeSort', values)
		print("MergeSorted == TimSorted", mergesorted == timsorted)

		input("Appuyez sur Entrer pour continuer")
	elif (choice == "2"):

		user_input = input("Tapez une phrase dans laquelle détecter des mots interdits:\n").split()
		user_input = [i.lower() for i in user_input]


		quicksorted = execute('QuickSort', user_input)
		mergesorted = execute('MergeSort', user_input)
		timsorted = execute('TimSort', user_input)

		sortedwords = timsorted

		terms = TermsToBlock()
		start = time.time()
		terms = random.sample(terms, len(terms))
		print("\nRandomisation de la liste de termes en {:0.4f} secondes - Nombre d'items dans la liste : {}".format(time.time() - start, len(terms)))
		print("Extrait : ", terms[:10], '\n')

		sortedterms = execute('QuickSort', terms)
		sortedterms = execute('MergeSort', terms)
		sortedterms = execute('TimSort', terms)

		compare.calls = 0
		begining = time.time()
		matches = WordsInListSearch (sortedwords, sortedterms, compare.call)
		print("Résultat de la recherche (en {:0.4f} secondes), {} comparaisons ({} mots, parmi une liste de {} mots)\n".format(time.time() - begining, compare.calls, len(sortedwords), len(sortedterms)), matches)

		input("Appuyez sur Entrer pour continuer")
	elif (choice == "3"):
		graph = {
			"a":[("b",3),("c",4)],
			"b":[('a',3),("d",3),("e",1)],
			"c":[('a',4),("e",2),("h",4)],
			"d":[('b',3),("e",6),("f",2)],
			"e":[('b',1),('c',2),('d',6),("g",4),("h",5)],
			"f":[('d',2),("g",3),("i",5)],
			"g":[('e',4),('f',3),("h",3),("i",3),("j",2)],
			"h":[('c',4),('e',5),('g',3),("j",3)],
			"i":[('f',5),('g',3),("j",3)],
			"j":[('h',3),('g',2),("i",3)],
		}
		print ('Valeurs du graph')
		for node, edges in graph.items():
			print(node, edges)

		choices = input("Sélectionner deux lettres à joindre dans le graph (ex:a,b) ")

		(start, end) = tuple(choices.split(','))
		(bestPath, bestCost) = FindBestPath(graph, start, end)

		print (bestPath, bestCost)

		input("Appuyez sur Entrer pour continuer")
	elif (choice == "q"):
		exit()




